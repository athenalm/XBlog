# XBlog

Blogging tools for XSite. Currently includes four main components:

* Appropriate templates and XSLT stylesheets for rendering a blog of a very particular style, including a somewhat bare-bones (for now) Atom feed.
* Some Python library modules for working with Web resources, in particular fetching HTTP(S), FTP(S), `file://`, and Gemini URIs, finding links in (X)HTML, XML, JSON, Gemini gemtext, and plain text, and finding (usually) all the outgoing links from a URI (including e.g. HTTP `Link` headers).
* An XSite plug-in for doing Webmention discovery and generating outgoing Webmentions based on links anywhere on your site (including non-blog posts), supported by a script that actually sends the mentions.
* A CGI script and supporting command-line scripts for receiving Webmentions, validating them, and extracting information from the source pages, accompanied by an XSLT stylesheet that generates decent-looking XHTML from that information.

These are licensed under the Affero General Public License version 3; this means if you use them, any code you combine with them will also have to fall under the AGPL. This does not include your website's content or CSS, but it may include other XSLT stylesheets, templates, and scripts that you use them with.

With that in mind, feel free to use these and adapt them as you see fit, as long as you follow the rules.
