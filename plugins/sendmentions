#!/usr/bin/env python3
# Part of XBlog Webmention support.
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Script to send outgoing mentions from the mentions.tosend file, deleting successfully-sent mentions from it and writing the sent mention information to mentions.sent.

SYNTAX: sendmentions tosend sent

tosend is the path to the mentions.tosend file.

sent is the path to the mentions.sent file.

This script is mostly intended for use as part of an automated rollout process with fixed arguments.
"""
import sys
import requests

not_sent = []
with open(sys.argv[1], "r") as tosend:
    with open(sys.argv[2], "a") as sent:
        for line in tosend:
            [source, target, endpoint] = line.rstrip("\n").split("\t")
            try:
                r = requests.post(endpoint, data={"source": source, "target": target})
                r.raise_for_status()
            except Exception as e:
                print("Error sending Webmention", e, file=sys.stderr)
                not_sent.append(line)
            sent.write(source + "\t" + target + "\n")

with open(sys.argv[1], "w") as f:
    f.writelines(not_sent)
