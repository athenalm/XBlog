# Part of XBlog Webmention support.
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path
import sys
import requests
import xsitelib
import contentlinks
import reslinks

def process_sidecar(document, config, params):
    unpublished = Path(config["resources"]) / "unpublished"

    sent_path = unpublished / "mentions.sent" # Read list of already sent mentions.
    try:
        with sent_path.open("r") as f:
            sent = f.readlines()
    except FileNotFoundError:
        sent = []

    nosend_path = unpublished / "mentions.nosend" # Read list of potential mention targets that don't support being mentioned.
    try:
        with nosend_path.open("r") as f:
            nosend = f.readlines()
    except FileNotFoundError:
        nosend = []

    doc_uri = xsitelib.document_uri(config, xsitelib.document_output_path(config, params["source-file"]))

    with (unpublished / "mentions.tosend").open("a") as tosend_file: # Append mode means no race condition when writing lines from many processes. See open(2) man page, O_APPEND.
        with nosend_path.open("a") as nosend_file:
            for link in contentlinks.find_xml_links(document, schemes=["http", "https", "ftp", "ftps", "file", "gemini"]): # Only HTML and XML documents and resources fetched over HTTP(S) will actually return rel values, but we'll try to discover on all supported schemes anyway.
                if ":" in link[0]: # Ignore relative links.
                    if doc_uri + "\t" + link[0] + "\n" not in sent and doc_uri + "\t" + link[0] + "\n" not in nosend: # Don't queue things already sent or already found not to support receiving mentions.
                        try:
                            mention_endpoints = reslinks.get_links(link[0], rels=["webmention", "http://webmention.org/"], typeid_order=[reslinks.typeid_protocol, reslinks.typeid_mimetypes, reslinks.typeid_magic])
                        except Exception as e:
                            print("Error finding Webmention endpoints", e, file=sys.stderr)
                            continue # We'll get this one later if it becomes available again, do the next one.

                        if len(mention_endpoints) > 0:
                            tosend_file.write(doc_uri + "\t" + link[0] + "\t" + mention_endpoints[0][0] + "\n") # FIXME: Duplicates can end up in mentions.tosend.
                        else: # No mention receive support, don't ask again.
                            nosend_file.write(doc_uri + "\t" + link[0] + "\n")
