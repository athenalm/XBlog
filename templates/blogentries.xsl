<?xml version="1.0" encoding="UTF-8"?>
<!--
XBlog, blogging for XSite.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?xsite-template name="main.xml"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="index-category" select="''" />
  <xsl:template match="dataset">
    <section>
      <h1>Entries</h1>
      <ul>
        <xsl:apply-templates />
      </ul>
    </section>
  </xsl:template>
  <xsl:template match="record">
    <xsl:if test="contains(@category, $index-category)">
      <li><em><xsl:value-of select="@date" /></em> - <a><xsl:attribute name="href"><xsl:value-of select="@path" /></xsl:attribute><xsl:value-of select="@title" /></a></li>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
