<?xml version="1.0" encoding="UTF-8"?>
<!--
XBlog, blogging for XSite.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/2005/Atom" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sl="https://www.alm.website/misc/specs/xslots">
  <param name="conf-site-base-url" />
  <xsl:template match="dataset">
    <sl:fragment>
      <updated><xsl:value-of select="./record[1]/@date" /></updated>
      <xsl:apply-templates />
    </sl:fragment>
  </xsl:template>
  <xsl:template match="record">
    <entry>
      <id><xsl:value-of select="$conf-site-base-url" /><xsl:value-of select="@path" /></id>
      <title><xsl:value-of select="@title" /></title>
      <updated><xsl:value-of select="@date" /></updated>
      <published><xsl:value-of select="@date" /></published>
      <category>
        <xsl:attribute name="term"><xsl:value-of select="@category" /></xsl:attribute>
      </category>
      <link rel="alternate">
        <xsl:attribute name="href"><xsl:value-of select="$conf-site-base-url" /><xsl:value-of select="@path" /></xsl:attribute>
      </link>
      <summary>
        <!-- TODO: Get summary; need documentinfo dataset with actual smarts? -->
        No summary is available for this entry due to technical incapacity.
      </summary>
    </entry>
  </xsl:template>
</xsl:stylesheet>
